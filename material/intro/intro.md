ODE: Introdução com o R
========================================================
author: Marcio Nicolau
date: 08-06-2015
font-import: http://fonts.googleapis.com/css?family=Nunito
font-family: 'Nunito'
autosize: true

Resumo
========================================================
type: subsection
incremental: true

- Analista / Embrapa Trigo :: Passo Fundo :: RS
- Graduação em Estatistica (2006)
- MBA Eng. Produção (2015)
- Experiencias
  - Modelagem Bayesiana 
  - Simulação
  - Estatística Aplicada
  - Modelos estruturados
  - Programação (C++/Java/C/JS)
  - R


Agenda
========================================================
type: subsection
incremental: true

- Preparar o ambiente de trabalho
  - Instalar o [R](http://www.r-project.org/), [RStudio](http://www.rstudio.com/products/rstudio/download/) e Bibliotecas
  - [Salvar repositório do curso](https://marcionicolau@bitbucket.org/marcionicolau/r-ode-course-2015)
- Introdução ODE
- [CRAN Task View: Differential Equations](http://cran.r-project.org/web/views/DifferentialEquations.html)
- Pacotes de trabalho
  - [deSolve](http://cran.r-project.org/web/packages/deSolve/index.html) / [rootSolve](http://cran.r-project.org/web/packages/rootSolve/index.html) / [bvpSolve](http://cran.r-project.org/web/packages/bvpSolve/index.html)
  - [simecol](http://cran.r-project.org/web/packages/simecol/index.html)
  - [FME](http://cran.r-project.org/web/packages/FME/index.html)
  - [odeintr](http://cran.r-project.org/web/packages/odeintr/index.html)

Formato do Curso
========================================================
incremental: true

- O Curso será oferecido no format "lecture"
- Uso de tutoriais e documentos 
- Vignettes
- Dividido em períodos (4)
  - Intro (atual)
  - ODE (básico) com o pacote deSolve, rootSolve e bvpSolve: formulação, uso, gráficos e sumários dos dados
  - ODE (avançado) com os pacotes simecol, FME e odeintr
  - Exemplos e considerações finais
  
Glossário e termos
========================================================
incremental: true

- Modelagem
  - ODE (Eq. Diff. Ord.)
  - IVP (Prob. Valor Inic.)
  - BVP (Prob. Valor Cont.)
- Programas
  - git (Controlador de versão)
  - Boost (Biblioteca C++)
  - VexCL (Biblioteca GPU/CPU/OpenCL/C++)

Dúvidas
========================================================
incremental: true

- Durante o curso
  - Levante a mão (a qualquer momento)
- Após o curso
  - marcio.nicolau@embrapa.br
  - skype: marcio.nicolau
  - twitter: @marcionicolau
  - URGENTE: (54) 3316-5933

Considerações iniciais
========================================================
incremental: true
source: ../../R/deSolve.R 6

- Conhecendo os participantes
- Expectativas
- Dados e/ou problemas para uso nos exemplos (período 4)
- Trabalhos com programação / modelagem
- Linguagens de programação
- **Vamos iniciar?**
