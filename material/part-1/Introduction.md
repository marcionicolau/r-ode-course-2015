Introdução a Equações Diferencias Ordinais no R
========================================================
author: Marcio Nicolau
date: 08-06-2015
font-import: http://fonts.googleapis.com/css?family=Nunito
font-family: 'Nunito'
autosize: true

Equações de Lorenz (1963)
========================================================
incremental: true

Descreve um sistema dinâmico caótico (representa a comportamento idealizado da atmosfera terrestre) formado por 3 equações

$$ 
  \begin{aligned}
  \frac{dX}{dt} &  = a * X + Y * Z \\
  \frac{dY}{dt} & = b * (Y - Z) \\
  \frac{dZ}{dt} & = -X * Y + c * Y - Z
  \end{aligned}
$$

Condições Iniciais

$$
X(0) = Y(0) = Z(0) = 1
$$

Equações de Lorenz (1963) - cont ...
========================================================
incremental: true

Onde $a, b$ e $c$ são parametros com valores $-8/3, -10$ e $28$ respectivamente.

Para representar este problema no R, precisamos definir duas partes:

- Especificação do Modelo
- Aplicação do Modelo

ODE no R - Especificação do Modelo
========================================================
incremental: true

- Parametros e Valores
- Variáveis de estado e condições iniciais
- Implementar as equações que calculam a taxa de mudança (p.ex $\frac{dX}{dt}$) das variáveis de estado

ODE no R - Aplicação do Modelo
========================================================
incremental: true

- Especificação do tempo no qual é esperado cada saída do modelo
- Integração das equações do modelo
- Gráfico dos resultados

Equações de Lorenz
========================================================
incremental: true

Especificação do Modelo


```r
parameters <- c(a = -8/3,
                b = -10,
                c = 28)
```
Variáveis de Estado 

```r
state <- c(X = 1,
           Y = 1,
           Z = 1)
```

Equações de Lorenz - Função
========================================================
incremental: true


```r
Lorenz<-function(t, state, parameters) {
  with(as.list(c(state, parameters)),{
    # rate of change
    dX <- a*X + Y*Z
    dY <- b * (Y-Z)
    dZ <- -X*Y + c*Y - Z

    # return the rate of change
    list(c(dX, dY, dZ))
  }) # end with(as.list ...
}
```

Especificação do Tempo

```r
times <- seq(0, 100, by = 0.01)
```

Equações de Lorenz - Integração do modelo
========================================================
incremental: true


```r
require(deSolve)
out <- ode(y = state, 
           times = times, func = Lorenz, 
           parms = parameters)
head(out)
```

```
     time         X        Y        Z
[1,] 0.00 1.0000000 1.000000 1.000000
[2,] 0.01 0.9848912 1.012567 1.259918
[3,] 0.02 0.9731148 1.048823 1.523999
[4,] 0.03 0.9651593 1.107207 1.798314
[5,] 0.04 0.9617377 1.186866 2.088545
[6,] 0.05 0.9638068 1.287555 2.400161
```

Equações de Lorenz - Gráficos
========================================================
incremental: true

<img src="Introduction-figure/unnamed-chunk-6-1.png" title="plot of chunk unnamed-chunk-6" alt="plot of chunk unnamed-chunk-6" width="750px" style="display: block; margin: auto;" />


Métodos para IVP em ODE
=========================================================
incremental: true

O pacote utilizado até o momento (deSolve) oferece vários métodos/algoritmos para solução de IVP em ODE.

O método padrão - baseado no código LSODA ODEPACK/FORTRAN é capaz de selecionar automaticamente entre sistemas "stiff" (autovalor do sistem é considerado grande) e "non-stiff".

Veja a seguir a diferença de tempo de execução para alguns dos métodos disponíveis

Métodos para IVP em ODE: Algoritmos
=========================================================
incremental: true

- RK4 - Runge Kutta 4a. Ordem
- LSODE
- LSODA 
- LSODES
- DASPK
- VODE




Métodos para IVP em ODE: Algoritmos (cont...)
=========================================================
incremental: true

Comparativo entre os métodos (tempo de execução)


```
    algo user.self sys.self elapsed
1    rk4     2.053    0.016   2.104
2  lsode     0.743    0.010   0.787
3  lsoda     1.008    0.006   1.027
4 lsodes     0.685    0.007   0.696
5  daspk     1.018    0.004   1.036
6   vode     0.675    0.003   0.683
```

