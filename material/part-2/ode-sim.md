ODE: Simulações no R com simecol, FME e odeintr
========================================================
author: Marcio Nicolau
date: 09-06-2015
font-import: http://fonts.googleapis.com/css?family=Nunito
font-family: 'Nunito'
autosize: true

ODE: Pacote simecol
========================================================
incremental: true

O conteúdo que vamos seguir nesta etapa esta no arquivo 'references/a-simecol-introduction.pdf' do repositório. Alguns usos do pacote simecol

- implementar, simular e compartilhar modelos
- usa o sistema de classes S4 do R
- Exemplos
  - Modelo Presa-Predador
  - Modelos em grid
  - Modelo Baseado em Idividuo (__water flea Daphnia__)
  
ODE: Pacote simecol (diferenciais)
========================================================
incremental: true

- Template ou modelo de implementação para modelos
- Vantagens para a comunidade científica
- Leitura facilitada devido a separação entre as equações do modelo e os outros componentes
- Protocolo padrão para descrever modelo baseado em individuo/agente (Grimm et al 2006)
- Alternativa viável
  - MATLAB
  - OSIRIS, SWARM
  
ODE: Pacote simecol (abordagem)
========================================================
source: ../../R/a-simecol-introduction.R 6

- Estado de Espaço
  - Dinamico (resp discr, cont.)
  - Estatico, sistemas independente do tempo

$$
  \begin{aligned}
  \dot x(t) & = f(t, x(t), u(t), p) \\
  y(t) & = g(t, x(t), u(t), p)
  \end{aligned}
$$

- Demos (R/a-simecol-introduction.R)

ODE: Pacote simecol (estrutura)
========================================================


```
function (time, init, parms) 
{
    x <- init
    p <- parms
    dx1 <- p["k1"] * x[1] - p["k2"] * x[1] * x[2]
    dx2 <- -p["k3"] * x[2] + p["k2"] * x[1] * x[2]
    list(c(dx1, dx2))
}
attr(,"source")
[1] "function (time, init, parms) {"                         
[2] "    x <- init"                                          
[3] "    p <- parms"                                         
[4] "    dx1 <-   p[\"k1\"] * x[1] - p[\"k2\"] * x[1] * x[2]"
[5] "    dx2 <- - p[\"k3\"] * x[2] + p[\"k2\"] * x[1] * x[2]"
[6] "    list(c(dx1, dx2))"                                  
[7] "  }"                                                    
```


```
 [1] "parms"     "init"      "observer"  "main"      "equations"
 [6] "times"     "inputs"    "solver"    "out"       "initfunc" 
```


