#include <Rcpp.h>
#include <random>

#include <boost/random.hpp>
#include <boost/generator_iterator.hpp>
#include <boost/random/normal_distribution.hpp>

// [[Rcpp::depends(BH)]]
// [[Rcpp::plugins("cpp11")]]

using namespace std;
using namespace Rcpp;

// [[Rcpp::export]]
NumericVector boostNormals(int n) {
  
  typedef boost::mt19937 RNGType; 	// select a generator, MT good default
  RNGType rng(123456);			// instantiate and seed
  
  boost::normal_distribution<> n01(0.0, 1.0);
  boost::variate_generator< RNGType, boost::normal_distribution<> > rngNormal(rng, n01);
  
  NumericVector V(n);
  for ( int i = 0; i < n; i++ ) {
    V[i] = rngNormal();
  };
  
  return V;
}


// [[Rcpp::export]]
NumericVector rcppNormals(int n) {
  return rnorm(n);
}


// [[Rcpp::export]]
NumericVector cxx11Normals(int n) {
  
  std::mt19937 engine(42);
  std::normal_distribution<> normal(0.0, 1.0);
  
  NumericVector V(n);
  for ( int i = 0; i < n; i++ ) {
    V[i] = normal(engine);
  };
  
  return V;
}

// You can include R code blocks in C++ files processed with sourceCpp
// (useful for testing and development). The R code will be automatically 
// run after the compilation.
//

/*** R
set.seed(42)
rcppNormals(10)

boostNormals(10)

cxx11Normals(10)

library(rbenchmark)

n <- 1e5

res <- benchmark(rcppNormals(n),
                 boostNormals(n),
                 cxx11Normals(n),
                 order="relative",
                 replications = 500)
print(res[,1:4])
*/
