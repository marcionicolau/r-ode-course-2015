// Use brandnew CRAN package BH for Boost headers

// [[Rcpp::depends(BH)]]
#include <Rcpp.h>

// One include file from Boost
#include <boost/date_time/gregorian/gregorian_types.hpp>

using namespace boost::gregorian;

// [[Rcpp::export]]
Rcpp::Date getIMMDate(int mon, int year) {
  // compute third Wednesday of given month / year
  date d = nth_day_of_the_week_in_month(nth_day_of_the_week_in_month::third,
                                        Wednesday, mon).get_date(year);
  date::ymd_type ymd = d.year_month_day();
  return Rcpp::wrap(Rcpp::Date(ymd.year, ymd.month, ymd.day));
}

/*** R
getIMMDate(3, 2013)

*/