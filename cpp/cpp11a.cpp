#include <Rcpp.h>

// Enable C++11 via this plugin (Rcpp 0.10.3 or later)
// [[Rcpp::plugins("cpp11")]]

using namespace Rcpp;

// This is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar). Learn
// more about Rcpp at:
//
//   http://www.rcpp.org/
//   http://adv-r.had.co.nz/Rcpp.html
//   http://gallery.rcpp.org/
//

// [[Rcpp::export]]
std::vector<double> transformEx(const std::vector<double>& x) {
  std::vector<double> y(x.size());
  std::transform(x.begin(), x.end(), y.begin(), 
                 [](double x) { return x*x; } );
  return y;
}

/*** R
x <- c(1,2,3,4)
transformEx(x)
*/
